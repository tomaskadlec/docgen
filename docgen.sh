#!/bin/sh

usage() {
	cat << --USAGE--

	Usage: $0 [-i IMAGE] [-t TAG] [-w DIR] -- ARGUMENTS

    -i IMAGE    Use image IMAGE (defaults to registry.gitlab.com/tomaskadlec/docgen)
    -t TAG      Select image tag (defaults to latest)
    -w DIR      Mount provided DIR instead of $PWD.
                Use it as working directory in the container.

    ARGUMENTS   Arguments that are passed to the image

--USAGE--
}

latest () {
    wget -O - -o /dev/null "https://gitlab.com/projects/${IMAGE#*/}/repository/tags" | grep -o 'tags/[.0-9]\+' | cut -d/ -f2 | head -1
}

IMAGE='registry.gitlab.com/tomaskadlec/docgen'
TAG=
DIR="$PWD"

while getopts "hi:t:p:" OPTION; do
  case $OPTION in
	i)
		IMAGE="$OPTARG"
		;;
	t)
		TAG="$OPTARG"
		;;
    w)
		DIR="$OPTARG"
		;;
  	*)
		echo "OPTION: $OPTION"
		usage
		exit 0
      	;;
  esac
done

shift $((OPTIND - 1))

if [ -z "$TAG" ]; then
    TAG=$(latest)
    TAG="${TAG#.*}"
fi

docker run -ti --rm \
    --env "CONTAINER_UID=$(id -u)" \
    --env "CONTAINER_GID=$(id -g)" \
    --volume="$DIR:/mnt" \
    --workdir="/mnt" \
    "$IMAGE:$TAG" "$@"

