FROM alpine:latest

ENV LANG C.UTF-8
ENV LANGUAGE C.UTF-8
ENV LC_ALL C.UTF-8

ENV GOSU_VERSION 1.10
RUN \
    apk add --no-cache --virtual .gosu-deps \
        dpkg \
        openssl \
    && dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true \
    && apk del .gosu-deps

RUN apk add --no-cache \
        bash \
        curl \
        openssh-client\
        python3

RUN mkdir -p /usr/local/docgen

ADD ["requirements.txt", "/usr/local/docgen/"]

RUN \
    sed '/^pkg-resources==0.0.0$/d' /usr/local/docgen/requirements.txt; \
    sed -i '/^pkg-resources==0.0.0$/d' /usr/local/docgen/requirements.txt; \
    pip3 install -r /usr/local/docgen/requirements.txt

ADD ["LICENSE", "*.py", "/usr/local/docgen/"]
ADD ["templates", "/usr/local/docgen/templates"]
ADD ["processors", "/usr/local/docgen/processors"]

RUN ln -s /usr/local/docgen/docgen.py /usr/local/bin/docgen

ADD ["entrypoint.sh","/usr/local/bin/"]
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/usr/local/bin/entrypoint.sh"]
CMD ["/bin/sh"]
