# processors/media.py
# Processes media and copies them to output directory maintaining source path
import click
import filecmp
from shutil import copyfile


from processors import Processor


class Media(Processor):
    """
    Copies media files to output directory
    """

    def supports(self, target):
                return super(Media, self).supports(target) and target["target"] == 'media'

    def process(self, target, data, config, **kwargs):
        # paths
        paths = self._resolve_paths(target.get("files", []), target.get("src"), target.get("dest"), recursive=True)
        # copy files
        for path in paths:
            try:
                src, dest = self._src_to_dest(path, dest=target.get("dest", "."), stem=False)
                if not dest.exists() or not filecmp.cmp(str(src), str(dest)):
                    self._dir(dest)
                    copyfile(str(src), str(dest))
                else:
                    click.echo("{}: Files {} and {} are equal. Doing nothing."
                               .format(self.__class__.__name__, src, dest), err=True)
            except Exception as ex:
                click.echo("{}: Failed to copy {} with error: {}"
                           .format(self.__class__.__name__, path, ex), err=True)
