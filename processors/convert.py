# processors/convert.py
# Convert ReST/Md files to output using a template

import click
from docutils.core import publish_parts, publish_programmatically
from docutils.io import StringInput, StringOutput
from docutils_tinyhtml import Writer
import markdown
from os.path import abspath, dirname, sep
from pathlib import Path
import re
from recommonmark.parser import CommonMarkParser
import sys


from processors import Processor


class Convert(Processor):
    """
    Loads and parses data

    configuration keys:
        target: convert
        template: base.html.j2
        src: ~
        dest: ~
        files:
            - intro.rst
            - input: study
              files:
                - study-actors.rst
                - study-needs.rst
                - study-requirements.rst
            - input: analysis
    """

    def __init__(self):
        self.helpers = [RstHelper(), MdHelper()]

    def supports(self, target):
        return super(Convert, self).supports(target) and target["target"] == 'convert'

    def process(self, target, data, config, **kwargs):
        environment = self._environment([Path(path) for path in target.get("templates", [])
                                         + config.get("templates", [])]
                                        + [Path(sys.argv[0]).resolve().parent / "templates"],
                                        kwargs.get("templating_extensions", []),
                                        target.get("templating_config", {}))
        default_template = target.get("template")
        if not default_template:
            raise RuntimeError("{}: Missing mandatory key template.".format(self.__class__.__name__))
        files = target.get("files", [])
        files = self._normalize(files, src_dir=target.get("src"), dest_dir=target.get("dest"))
        # read document titles
        self._titles(files)
        # convert input data to output documents
        for index, file in enumerate(files):
            try:
                file["index"] = index
                template = environment.get_template(file.get("template", default_template))
                src, dest = self._src_to_dest(file["input"], target.get("dest"),
                                              stem=False, suffix=target.get("suffix"))

                if target.get("skip_existing", False) and dest.exists():
                    click.echo("{}: File {} exists. Doing nothing."
                               .format(self.__class__.__name__, dest), err=True)
                    continue

                self._dir(dest)
                dest.write_text(
                    template.render({
                        "current": file,
                        "document": self._input(file["input"]),
                        "meta": target.get("meta", data.get("meta")),
                        "toc": target["files"],
                        "nav": files,
                        "toc_title": target.get("toc_title"),
                        "nav_prev": target.get("nav_prev", "Previous"),
                        "nav_next": target.get("nav_next", "Next"),
                        "doc_title": target.get("doc_title"),
                        "data": data
                    })
                )
            except Exception as ex:
                click.echo("{}: Conversion of {} failed with error: {}".format(self.__class__.__name__,
                                                                               file.get("input"), ex))

    def _normalize(self, files, src_dir=None, dest_dir=None, parent=None, level=0):
        result = list()
        for index, file in enumerate(files):
            if not isinstance(file, dict):
                file = files[index] = {"input": file}
            try:
                file["input"] = self._resolve_path(file.get("input"), src_dir, dest_dir)
            except RuntimeWarning as ex:
                click.echo("{}".format(ex), err=True)
                continue
            file["level"] = level
            file["parent"] = parent
            result.append(file)
            if "files" in file and isinstance(file["files"], list):
                result.extend(self._normalize(file["files"], src_dir=src_dir, dest_dir=dest_dir,
                                              parent=file, level=level + 1))
        return result

    def _titles(self, files):
        for index, file in enumerate(files):
            try:
                if not file.get("title"):
                    document = self._input(file["input"])
                    file["title"] = document.get("title") if document.get("title") else file["input"].name
            except Exception as ex:
                click.echo("{}: Reading title of {} failed with error: {}".format(self.__class__.__name__,
                                                                                  file.get("input"), ex))
        return files

    def _input(self, file):
        for helper in self.helpers:
            if helper.supports(file):
                return helper.handle(file)


class RstHelper(object):
    """
    Helps with processing files in ReStructured Text format
    """

    @staticmethod
    def supports(file):
        return str(file).endswith(".rst")

    def handle(self, file):
        """
        :param file:
        """
        parts = self._publish_output(file.read_text())
        return {
            "title": parts["title"],
            "toc": self._document_toc(parts["sections"])[1],
            "content": parts["body"],
            "citations": parts["html_citations"],
            "footnotes": parts["html_footnotes"],
        }

    @staticmethod
    def _publish_output(content):
        writer = Writer()
        return publish_parts(source=content,
                             writer=writer,
                             writer_name="html",
                             settings_overrides={"no_system_messages": True})

    @classmethod
    def _document_toc(cls, toc):
        result = []
        previous = None
        while toc:
            current = list(toc[0])
            # level down
            if previous and previous[0] < current[0]:
                toc, partial_result = cls._document_toc(toc)
                previous.append(partial_result)
            # level up
            elif previous and previous[0] > current[0]:
                return toc, result
            # same level
            else:
                previous = current
                result.append(current)
                toc = toc[1:]
        return toc, result


class MdHelper(RstHelper):
    """
    Helps with processing files in MarkDown format
    """

    @staticmethod
    def supports(file):
        return str(file).endswith(".md")

    @staticmethod
    def _publish_output(content):
        content = MdHelper._process_tables(content)
        writer = Writer()
        output, pub = publish_programmatically(
            source=content, source_class=StringInput, source_path=None,
            destination=None, destination_path=None, destination_class=StringOutput,
            reader=None, reader_name='standalone',
            parser=CommonMarkParser(), parser_name='markdown',
            writer=writer, writer_name='html',
            settings=None, settings_spec=None,
            settings_overrides={"no_system_messages": True},
            config_section=None,
            enable_exit_status=False)
        return pub.writer.parts

    @staticmethod
    def _process_tables(source):
        """
        Convert markdown tables to html, since recommonmark can't. This requires 3 steps:
            Snip out table sections from the markdown
            Convert them to html
            Replace the old markdown table with an html table
        """
        md = markdown.Markdown(extensions=['markdown.extensions.tables'])
        table_processor = markdown.extensions.tables.TableProcessor(md.parser)

        blocks = re.split(r'\n{2,}', source)

        for i, block in enumerate(blocks):
            if table_processor.test(None, block):
                html = md.convert(block)
                styled = html.replace('<table>', '<table border="1" class="docutils">', 1)  # apply styling
                blocks[i] = styled

        # re-assemble into markdown-with-tables-replaced
        return '\n\n'.join(blocks)
