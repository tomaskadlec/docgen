# processors/__init__.py
#

from abc import ABCMeta, abstractmethod
import click
from jinja2 import Environment, FileSystemLoader
import json
from os import getcwd, makedirs, walk
from os.path import abspath, basename, dirname, relpath, sep
from pathlib import Path
import unicodedata


class Processor(metaclass=ABCMeta):
    """
    A base class each processor must inherit from.
    """

    def supports(self, target):
        """
        Check if target is supported
        :param target: target being processed
        :return: True is target is supported False otherwise
        """
        return "target" in target

    @abstractmethod
    def process(self, target, data, config, **kwargs):
        raise RuntimeError("{}.process() is not implemented.".format(self.__class__.__name__))

    def _resolve_paths(self, patterns, src_dir=None, dest_dir=None, recursive=False):
        """
        Resolve patterns to valid paths
        :param patterns: list of patterns to resolve
        :param src_dir: use this directory instead of curr
        :param dest_dir: if argument is set exclude files that are in destination directory
        :param recursive: if result is a directory add all files it contains
        :return: list of paths to process
        """
        # absolute path to source dir
        src_dir = Path(getcwd()) / src_dir if src_dir else Path(getcwd())
        # compute all paths from provided list of patterns
        paths = list()
        for pattern in patterns:
            try:
                paths_len = len(paths)
                paths.extend(src_dir.glob(pattern))
                if len(paths) <= paths_len:
                    click.echo("{}: Result of pattern {} is no files."
                               .format(self.__class__.__name__, pattern), err=True)
            except Exception as ex:
                click.echo("{}: Pattern {} resulted in error: {}.".format(self.__class__.__name__, pattern, ex))
        # remove path in destination directory
        if dest_dir:
            dest_dir = abspath(dest_dir)
            paths = [path for path in paths if not str(path).startswith(dest_dir)]
        # directories in result
        if recursive:
            dirs = [path for path in paths if path.is_dir()]
            paths = [path for path in paths if path.is_file()]
            for dir in dirs:
                result = self._resolve_dir(dir)
                paths.extend(result)
        else:
            paths = filter(lambda path: path.is_file(), paths)
        # resolved paths
        return paths

    def _resolve_path(self, path, src_dir=None, dest_dir=None):
        if not path:
            raise RuntimeWarning("{}: Empty path is not allowed.".format(self.__class__.__name__))
        # absolute path to source dir
        src_dir = Path(getcwd()) / src_dir if src_dir else Path(getcwd())
        path = src_dir / path
        # path must exist
        if not path.exists():
            raise RuntimeWarning("{}: Path {} does not exist.".format(self.__class__.__name__, path))
        # and be a regular file
        if not path.is_file():
            raise RuntimeWarning("{}: Path {} is not a regular file.".format(self.__class__.__name__, path))
        # and optionally not in dest_dir
        if dest_dir and str(path).startswith(abspath(dest_dir)):
            raise RuntimeWarning("{}: Path {} is in destination directory.".format(self.__class__.__name__, path))
        # resolved path
        return path

    @staticmethod
    def _resolve_dir(dir):
        paths = list()
        for root, dirs, files in walk(str(dir)):
            paths.extend([Path(root) / file for file in files])
        return paths

    def _src_to_dest(self, path, dest=None, stem=True, suffix=None, cwd=Path(getcwd())):
        src = path.relative_to(cwd)
        if not dest:
            dest = "."
        if stem:
            dest = cwd / dest / src.parent / src.stem
        else:
            dest = cwd / dest / src
        if suffix:
            dest = Path("{}.{}".format(dest, suffix))
        dest = dest.relative_to(cwd)
        click.echo("{}: Processing {} => {}".format(self.__class__.__name__, src, dest), err=True)
        return src, dest

    def _dir(self, path):
        if not path.parent.is_dir():
            click.echo("{}: Create directory {}".format(self.__class__.__name__, path.parent), err=True)
            makedirs(str(path.parent))

    @staticmethod
    def _environment(paths, templating_extensions=[], templating_config={}):
        paths = [str(path.parent) if path.is_file() else str(path) for path in paths]
        environment = Environment(loader=FileSystemLoader(paths), **templating_config)
        environment.filters['to_json'] = _to_json
        environment.filters['to_yaml'] = _to_json
        environment.filters['relpath'] = _relpath
        environment.filters['prev'] = _prev
        environment.filters['next'] = _next
        environment.filters['title_to_anchor'] = _title_to_anchor
        for templating_extension in templating_extensions:
            templating_extension(environment)
        return environment


def _to_json(node):
    """
    Jinja2 filter. Returns an object serialized in JSON format
    :param node:
    :return:
    """
    return json.dumps(node)


def _relpath(path_to, path_from):
    """
    Jinja2 filter. Returns relative path between two paths
    :param path_to:
    :param path_from:
    :return:
    """
    try:
        dirname_to = dirname(str(path_to))
        if not dirname_to:
            dirname_to = "."
        return "{}{}{}".format(
            relpath(dirname_to, dirname(str(path_from))),
            sep,
            basename(str(path_to))
        )
    except ValueError as ex:
        return str(path_to)


def _next(linear_list, current):
    try:
        next = linear_list[current["index"] + 1]
        return _relpath(next["input"], current["input"])
    except:
        return False


def _prev(linear_list, current):
    try:
        prev = linear_list[current["index"] - 1] if current["index"] > 0 else None
        return _relpath(prev["input"], current["input"])
    except:
        return False


def _title_to_anchor(title):
    nfkd_form = unicodedata.normalize('NFKD', title.replace(" ", "-").lower())
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])
