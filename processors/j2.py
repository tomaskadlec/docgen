# processors/j2.py
# Process .j2 and outputs appropriate files
import click
from jsonpointer import resolve_pointer
from os import getcwd
from pathlib import Path
from ruamel import yaml
import sys

from processors import Processor


class J2(Processor):
    """
    Loads and parses data

    configuration keys:
        target: j2 | template
        src: ~
        dest: ~
        files:
            - **/*.j2
        stem: yes
    """

    def supports(self, target):
        return super(J2, self).supports(target) and (target["target"] == 'j2' or target["target"] == 'template')

    def process(self, target, data, config, **kwargs):
        # paths
        paths = self._resolve_paths(target.get("files", ["**/*.j2"]),
                                    target.get("src"), target.get("dest"), recursive=True)
        # jinja2 environment
        environment = self._environment([Path(getcwd())], kwargs.get("templating_extensions", []),
                                        target.get("templating_config", {}))
        # generate output
        for path in paths:
            src, dest = self._src_to_dest(path, dest=target.get("dest", "."), stem=target.get("stem", True))

            if target.get("skip_existing", False) and dest.exists():
                click.echo("{}: File {} exists. Doing nothing."
                           .format(self.__class__.__name__, dest), err=True)
                continue

            self._dir(dest)
            template = environment.get_template(str(src))
            dest.write_text(template.render(data))


class TemplateData(Processor):
    """
    Loads and parses data

    configuration keys:
        target: template-data
        skip_existing: yes
        src: ~
        dest: ~
        templates:
            - dirname(PATH/TO/docgen.py) + /templates # default, always added
            - ...
        template: structured-output.html.j2
        files:
            - **/*.rst.yml
            - **/*.md.yml
        stem: yes
    """

    def supports(self, target):
        return super(TemplateData, self).supports(target) and target["target"] == 'template-data'

    def process(self, target, data, config, **kwargs):
        # paths
        paths = self._resolve_paths(target.get("files", ["**/*.rst.yml", "**/*.md.yml"]),
                                    target.get("src"), target.get("dest"), recursive=True)
        # jinja2 environment
        environment = self._environment([Path(path) for path in target.get("templates", []) +
                                         config.get("templates", [])] + [Path(sys.argv[0]).parent / "templates"],
                                        kwargs.get("templating_extensions", []),
                                        target.get("templating_config", {}))
        # generate output
        try:
            template = environment.get_template(target.get("template"))
        except Exception as ex:
            raise RuntimeError("{}: Failed to load template {} with error: {}"
                               .format(self.__class__.__name__, target.get("template"), ex))
        for path in paths:
            try:
                data["tpl_data"] = yaml.safe_load(path.read_text())
                src, dest = self._src_to_dest(path, dest=target.get("dest", "."), stem=target.get("stem", True))

                if target.get("skip_existing", False) and dest.exists():
                    click.echo("{}: File {} exists. Doing nothing."
                               .format(self.__class__.__name__, dest), err=True)
                    continue

                self._dir(dest)
                dest.write_text(template.render(data))
            except Exception as ex:
                click.echo("{}: Failed to output {} with error: {}"
                           .format(self.__class__.__name__, path, ex), err=True)


class CollectionData(Processor):
    """
    Renders data collection(s) identified by pointers using a preset template

    configuration keys:
        target: collection-data
        skip_existing: yes
        dest: ~
        templates:
            - dirname(PATH/TO/docgen.py) + /templates # default, always added
            - ...
        template: rendered-data.html.j2
        pointers:
            - **/*.rst.yml
            - **/*.md.yml
        stem: yes
    """

    def supports(self, target):
        return super(CollectionData, self).supports(target) and target["target"] == 'collection-data'

    def process(self, target, data, config, **kwargs):
        # jinja2 environment
        environment = self._environment([Path(path) for path in target.get("templates", []) +
                                         config.get("templates", [])] + [Path(sys.argv[0]).parent / "templates"],
                                        kwargs.get("templating_extensions", []),
                                        target.get("templating_config", {}))
        # generate output
        try:
            template = environment.get_template(target.get("template"))
        except Exception as ex:
            raise RuntimeError("{}: Failed to load template {} with error: {}"
                               .format(self.__class__.__name__, target.get("template"), ex))
        for pointer in target.get("pointers", []):
            collection = resolve_pointer(data, pointer, None)
            if not collection or not isinstance(collection, dict):
                click.echo("{}: Pointer {} did not resolve to collection or collection is empty."
                           .format(self.__class__.__name__, pointer), err=True)
                continue
            for id, item in collection.items():
                dest = Path(getcwd()) / target.get("dest", "") / str(id)
                dest = dest.with_suffix(target.get("suffix")) if target.get("suffix") else dest
                click.echo("{}: Processing {}/{} => {}".format(self.__class__.__name__, pointer, id, dest), err=True)
                try:
                    if target.get("skip_existing", False) and dest.exists():
                        click.echo("{}: File {} exists. Doing nothing."
                                   .format(self.__class__.__name__, dest), err=True)
                        continue

                    self._dir(dest)
                    data["collection_item"] = item
                    dest.write_text(template.render(data))
                except Exception as ex:
                    click.echo("{}: Failed to output {} with error: {}"
                               .format(self.__class__.__name__, dest, ex), err=True)
                    raise
