# processors/info.py
# Provides information about output

import click
import json
from os import getcwd
from pathlib import Path


from processors import Processor


class Info(Processor):
    """
    Loads and parses data
    """

    def supports(self, target):
        return super(Info, self).supports(target) and target["target"] == 'info'

    def process(self, target, data, config, **kwargs):
        try:
            dest = Path(getcwd())
            if target.get("dest"):
                dest = dest / target.get("dest") / "info.yml"
            self._dir(dest)
            dest.write_text(json.dumps(config.get("meta")))
            click.echo("{}: File {} written.".format(self.__class__.__name__, dest.relative_to(getcwd())), err=True)
        except Exception as ex:
            click.echo("{}: Error {}".format(self.__class__.__name__, ex), err=True)
