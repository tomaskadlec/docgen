# README

Parse Markdown with tables.

Atribut | Datový typ | Popis
------------ | ------------- | -------------
enabled | bool | příznak jestli je daná konfigurace v provozu
from | array | -
bcc | array | -
replyTo | array |
transport | string | způsob doručení (SMTP, Google atd.)
username | string | přihlašovací jméno k serveru
password | string | heslo
host | string | hostitel e-mail serveru
port | integer | port e-mail serveru
encryption | string | způsob šifrování
authMode | string | způsob autentizace
deliveryAddress | string | -
disableDelivery | bool | -
