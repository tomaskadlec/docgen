# tests/extensions/ext1/ext11.py
import click
from processors import Processor


class Ext11Processor(Processor):

    def supports(self, target):
        return super().supports(target) and target["target"] == "ext11"

    def process(self, target, data, config, **kwargs):
        click.echo("{}: Extension is working.".format(self.__class__.__name__), err=True)


def register_processors():
    return [Ext11Processor()]
