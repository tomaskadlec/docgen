# tests/extensions/ext1/ext11.py
import click
from processors import Processor


def register_templating(environment):
    environment.filters["ext2_upper"] = _ext2_upper


def _ext2_upper(text):
    return str(text).upper()
