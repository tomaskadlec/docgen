=====================================================
022-analysis
=====================================================

:Info: See <http://docutils.sf.net/rst.html> for introductory docs.
:Author: David Goodger <goodger@python.org>
:Description: This is a "docinfo block", or bibliographic field list

.. NOTE:: If you are reading this as HTML, please read
   `<cheatsheet.txt>`_ instead to see the input syntax examples!

Section Structur1
=================

Section titles are underlined or overlined & underlined.

Section Structu12
-----------------

Section titles are underlined or overlined & underlined.

Section Structur2
=================

Section titles are underlined or overlined & underlined.

Section Structu21
-----------------

Section titles are underlined or overlined & underlined.
