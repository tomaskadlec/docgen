#!/usr/bin/env python3
# docgen command

import click
from importlib import import_module
from os import chdir, getcwd
from pathlib import Path
import sys


from data import Data
from processors.convert import Convert
from processors.info import Info
from processors.j2 import CollectionData, J2, TemplateData
from processors.media import Media


processors = [Info(), Convert(), J2(), Media(), TemplateData(), CollectionData()]


@click.group()
def cli():
    pass


@cli.command()
@click.option('--config', nargs=1, default="docgen.yml")
@click.option('--extra-vars', nargs=1, default=None, multiple=True)
@click.option('--working-directory', nargs=1, default=None)
@click.argument('paths', nargs=-1, required=False)
def run(config, extra_vars, paths, working_directory):
    """
    Loads data and runs all registered processors to produce output.

    Options:

        --config=PATH
            Path to a configuration file (defaults to docgen.yml)

            All paths used in configuration file must be relative to current working
            directory.

        --extra-vars=KEY=VALUE
            Override configuration values, e.g.:
                --vars=docgen.html.template=my_template.html.j2
            Option may be used multiple times.

        --working-directory=DIR
            If provided current working directory is changed to DIR.

    Arguments:

        PATHS list of paths where to look for data (files or directories)
            If path is a directory globbing is used to find all files with yml extension
            (**/*.yml).
            Argument is optional.
    """
    # load data and configuration
    paths = list(paths)
    paths.append(config)
    data = Data(paths, extra_vars)
    # load extensions
    templating_extensions = _register_extensions(data.data["docgen"].get("extensions", []))
    # chdir if requested
    try:
        chdir(working_directory) if working_directory else None
    except Exception as ex:
        click.echo("Failed to change CWD to {} with error {}".format(working_directory, ex), err=True)
        sys.exit(1)
    # run processors
    if "targets" in data.data["docgen"]:
        for id, target in data.data["docgen"]["targets"].items():
            for processor in processors:
                if processor.supports(target):
                    click.echo("=== Target: {} ===".format(id), err=True)
                    processor.process(target, data.data, data.data["docgen"],
                                      templating_extensions=templating_extensions)
                    break


def _register_extensions(extensions):
    templating_extensions = list()
    for path in extensions:
        path = Path(path)
        if not path.exists():
            click.echo("Extensions: Failed to load {}".format(path), err=True)
            continue
        elif path.is_dir():
            for file in path.glob("*.py"):
                templating_extensions.append(_register_extension(file))
        elif path.is_file():
            templating_extensions.append(_register_extension(path))

    templating_extensions = [x for x in templating_extensions if x]
    return templating_extensions


def _register_extension(path):
    templating_extension = None
    try:
        if not path.is_absolute():
            path = Path(getcwd()) / path
        if str(path.parent) not in sys.path:
            sys.path.append(str(path.parent))
        ext = import_module(path.stem)
        if hasattr(ext, "register_processors"):
            processors.extend(ext.register_processors())
        if hasattr(ext, "register_templating"):
            templating_extension = ext.register_templating
        click.echo("Extensions: Loaded {}".format(path), err=True)
    except Exception as ex:
        click.echo("Extensions: {} failed to load with error: {}".format(path, ex), err=True)
    return templating_extension


if __name__ == '__main__':
    cli()
