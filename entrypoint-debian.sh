#!/bin/sh
# Container entrypoint

## User and group

# Handle group
if [ -z "$CONTAINER_GID" ]; then
    export CONTAINER_GID=$(getent group 'nobody' | cut -d: -f 3)
elif ! getent group "$CONTAINER_GID"; then
    groupadd -g "$CONTAINER_GID" container_group
fi
export CONTAINER_GROUP=$(getent group "$CONTAINER_GID" | cut -d: -f1)

# Handle user
if [ -z "$CONTAINER_UID" ]; then
    export CONTAINER_UID=$(getent passwd 'nobody' | cut -d: -f 3)
elif ! getent passwd "$CONTAINER_UID"; then
    useradd -g "$CONTAINER_GROUP" -u "$CONTAINER_UID" container_user
fi
export CONTAINER_USER=$(getent passwd "$CONTAINER_UID" | cut -d: -f 1)

## Run command using gosu

# run CMD or user defined command
echo "container_init: GOSU $CONTAINER_USER($CONTAINER_UID) $CONTAINER_GROUP($CONTAINER_GID)"
echo "container_init: CMD $@"
exec gosu "$CONTAINER_USER" "$@"
