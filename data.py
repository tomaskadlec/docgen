# tools/data.py
# Loads data into a dict and optionally validates the data

import click
from jsonpointer import set_pointer
from os.path import abspath, dirname
from pathlib import Path
import ruamel.yaml
import sys


class Data(object):
    """
    Loads and parses data
    """

    def __init__(self, paths, extra_vars):
        # initialize data
        self.data = dict()
        # root directory
        self.root = abspath(dirname(sys.argv[0]))
        # compute file paths and then load them one by one
        for path in self._paths(paths):
            self.data = self._merge(
                self.data,
                self._scope(path.parent.parts, self._load(path))
            )
        # process vars
        self._vars(extra_vars)
        # validate loaded data
        # FIXME implementation

    def merge(self, data):
        """
        Merges arbitrary data into current data
        :param data:
        :return:
        """
        self.data = self._merge(self.data, data)

    @classmethod
    def _merge(cls, original, update):
        """
        Recursively merges dict update into original dict.
        """
        for key, value in update.items():
            if isinstance(value, dict) and key in original and isinstance(original[key], dict):
                original[key] = cls._merge(original[key], value)
            else:
                original[key] = value
        return original

    @classmethod
    def _scope(cls, keys, data):
        try:
            return {keys[0]: cls._scope(keys[1:], data)}
        except:
            return data

    @staticmethod
    def _load(path):
        try:
            # skip hidden files
            if path.name.startswith('.'):
                return {}
            click.echo("Loading file {}".format(path), err=True)
            return ruamel.yaml.safe_load(path.read_text())
        except Exception as ex:
            click.echo("Path: {}\n{}".format(path, ex), err=True)
            return {}

    @staticmethod
    def _paths(paths):
        """
        Returns a set of file paths to be loaded
        :param paths:
        :return:
        """
        result = list()
        for path in paths:
            path = Path(path)
            if path.is_file():
                result.append(path)
            elif path.is_dir():
                result.extend(list(path.glob("**/*.yml")))
            else:
                # FIXME log error rather than writing to stdout
                click.echo("Path {} does not exist.".format(path), err=True)
        return result

    def _vars(self, extra_vars):
        for extra_var in extra_vars:
            try:
                key, value = extra_var.split("=")
                key = "/{}".format("/".join(key.split(".")))
                set_pointer(self.data, key, value)
            except Exception as ex:
                click.echo("Data: Failed to process extra var: {} ({})".format(extra_var, ex))
